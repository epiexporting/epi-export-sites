﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Bonnier.Bt.MultiSite.PageTypes;
using Bonnier.Core.Common;

using EPiServer;
using EPiServer.Core;

using ExportHelper;
using ExportHelper.Entities;
using ExportHelper.Utils;

namespace Bonnier.Bt.MultiSite.Web
{
    public class ExportArticleXmlValidator : ExportXmlValidator<ArticlePageData>
    {
        /*public ExportArticleXmlValidator() : base(ExportSettings.TargetDirectory)
        {
        }*/

        protected override void ValidatePost(ArticlePageData pageData, PostXmlObject post)
        {
            ValidateHeadline(pageData, post);
            ValidateType(pageData, post);
            ValidateImportId(pageData, post);
            ValidateUrl(pageData, post);
            ValidatePermalink(pageData, post);
            ValidateDate(pageData, post);
            ValidateExcerpt(pageData, post);
            ValidateUsers(pageData, post);
            ValidateImage(pageData, post);
        }

        protected void ValidateHeadline(ArticlePageData pageData, PostXmlObject post)
        {
            if (pageData.PageName != post.Title)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": title doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, pageData.GridHeader, post.Title));
            }
        }

        protected void ValidateType(ArticlePageData pageData, PostXmlObject post)
        {
            if ("Article" != post.Type)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": type doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, "Article", post.Type));
            }
        }

        protected void ValidateImportId(ArticlePageData pageData, PostXmlObject post)
        {
            if (!string.IsNullOrEmpty(post.ImportId))
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": ImportId is not empty. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, string.Empty, post.ImportId));
            }
        }

        protected void ValidateUrl(ArticlePageData pageData, PostXmlObject post)
        {
            var pageUrlBuilder = new UrlBuilder(pageData.LinkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(pageUrlBuilder, pageData.PageLink, UTF8Encoding.UTF8);

            var postUrlBuilder = new UrlBuilder(post.Url);
            if (postUrlBuilder.Host != ExportSettingsHelper.UrlHost)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": URL host is invalid. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, ExportSettingsHelper.UrlHost, postUrlBuilder.Host));
            }

            if (pageUrlBuilder.Path != postUrlBuilder.Path)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": URL path is invalid. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, pageUrlBuilder.Path, postUrlBuilder.Path));
            }

            if (pageUrlBuilder.Query != postUrlBuilder.Query)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": URL query is invalid. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, pageUrlBuilder.Query, postUrlBuilder.Query));
            }
        }

        protected void ValidatePermalink(ArticlePageData pageData, PostXmlObject post)
        {
            if (pageData.URLSegment != post.Permalink)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": URLSegment doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, pageData.URLSegment, post.Permalink));
            }
        }

        protected void ValidateDate(ArticlePageData pageData, PostXmlObject post)
        {
            if (pageData.Created.ToString("s") != post.Date.ToString("s"))
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": Date doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, pageData.Created, post.Date));
            }
        }

        protected void ValidateContent(ArticlePageData pageData, PostXmlObject post)
        {
            if (pageData.MainBodyCore == null)
            {
                if (post.Content != null)
                {
                    throw new Exception(string.Format("Validation failed for page with id \"{0}\": Content doesn't match. Expected: null; Actual: \"{1}\"", pageData.PageLink, post.Content));
                }
                return;
            }

            var mainBody = pageData.MainBodyCore;
            if (mainBody != post.Content)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": Content doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, mainBody, post.Content));
            }
        }

        protected void ValidateExcerpt(ArticlePageData pageData, PostXmlObject post)
        {
            if (pageData.PreambleCore == null)
            {
                if (post.Excerpt != null)
                {
                    throw new Exception(string.Format("Validation failed for page with id \"{0}\": Excerpt doesn't match. Expected: null; Actual: \"{1}\"", pageData.PageLink, post.Excerpt));
                }
                return;
            }

            var preamble = pageData.PreambleCore;
            if (preamble != post.Excerpt)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": Excerpt doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, preamble, post.Excerpt));
            }
        }

        protected void ValidateUsers(ArticlePageData pageData, PostXmlObject post)
        {
            var authors = GetArticleAuthors(pageData);
            if (authors == null || authors.Count == 0)
            {
                if (post.Users != null && post.Users.Count != 0)
                {
                    throw new Exception(string.Format("Validation failed for page with id \"{0}\": Users doesn't match. Expected: no users; Actual count: \"{1}\"", pageData.PageLink, post.Users.Count));
                }
                return;
            }

            if (authors.Count != post.Users.Count)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": Users doesn't match. Expected count: \"{1}\"; Actual count: \"{2}\"", pageData.PageLink, authors.Count, post.Users.Count));
            }

            foreach (var author in authors)
            {
                var localAuthor = author;
                var postUsers = post.Users.Where(u => u.Name == localAuthor.Name).ToList();
                if (postUsers.Count == 0)
                {
                    throw new Exception(string.Format("Validation failed for page with id \"{0}\": User was not found: \"{1}\"", pageData.PageLink, author));
                }

                if (postUsers.Count > 1)
                {
                    throw new Exception(string.Format("Validation failed for page with id \"{0}\": User occurred more than 1 time: \"{1}\"", pageData.PageLink, author));
                }

                if (postUsers.First().Email != localAuthor.Email)
                {
                    throw new Exception(string.Format("Validation failed for page with id \"{0}\": User has a wrong email. Expected result: \"{1}\"; Actual count: \"{2}\"", pageData.PageLink, localAuthor.Email, postUsers.First().Email));
                }
            }
        }

        protected void ValidateImage(ArticlePageData pageData, PostXmlObject post)
        {
            var expectedImageUrl = pageData.ImageCore ?? string.Empty;
            var actualImageUrl = (post.RichContent == null || post.RichContent.Image == null || string.IsNullOrEmpty(post.RichContent.Image.Url)) ? string.Empty : post.RichContent.Image.Url;
            if (expectedImageUrl != actualImageUrl)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": Image URL doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, expectedImageUrl, actualImageUrl));
            }

            var expectedPhotographers = pageData.ImagePhotographerCore ?? string.Empty;
            var actualPhotographers = (post.RichContent == null || post.RichContent.Image == null || post.RichContent.Image.Photographers == null || post.RichContent.Image.Photographers.Count == 0) ? string.Empty : post.RichContent.Image.Photographers.Single();
            if (expectedPhotographers != actualPhotographers)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": Image photographers doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, expectedPhotographers, actualPhotographers));
            }

            var expectedTitle = pageData.ImageTextCore ?? string.Empty;
            var actualTitle = (post.RichContent == null || post.RichContent.Image == null || string.IsNullOrEmpty(post.RichContent.Image.Title)) ? string.Empty : post.RichContent.Image.Title;
            if (expectedTitle != actualTitle)
            {
                throw new Exception(string.Format("Validation failed for page with id \"{0}\": Image title doesn't match. Expected: \"{1}\"; Actual: \"{2}\"", pageData.PageLink, expectedTitle, actualTitle));
            }
        }

        private List<PostUserXmlObject> GetArticleAuthors(ArticlePageData articlePageData)
        {
            if ((articlePageData.AuthorsCore == null || articlePageData.AuthorsCore.Count == 0) && string.IsNullOrEmpty(articlePageData.AltAuthorCore))
            {
                return null;
            }

            var result = new List<PostUserXmlObject>();
            foreach (var author in articlePageData.AuthorsCore)
            {
                var authorPage = author.LoadPage<AuthorPageData>();
                var userXmlObject = new PostUserXmlObject
                {
                    Name = authorPage.PageName,
                    Email = string.IsNullOrEmpty(authorPage.EmailCore) ? null : authorPage.EmailCore
                };
                result.Add(userXmlObject);
            }
            if (!string.IsNullOrEmpty(articlePageData.AltAuthorCore))
            {
                var userXmlObject = new PostUserXmlObject
                {
                    Name = articlePageData.AltAuthorCore
                };
                result.Add(userXmlObject);
            }

            return result;
        }
    }
}