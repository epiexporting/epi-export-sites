﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportPage.aspx.cs" Inherits="Bonnier.Bt.MultiSite.Web.ExportPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Export page</title>
</head>
<style>
    .headline {
        font-size: 30px;
        font-weight: bold;
    }
    .success {
        color: #C2D985;
    }
    .failed {
        color: #E3675C;
    }
    .error-message-container {
        border: 3px solid #E3675C;
    }
    .body {
        font-size: 15px;
    }
    .results-header {
        font-size: 22px;
        font-weight: bold;
    }
    </style>
<body>
    <form id="form1" runat="server">
    <div>
        <p>
            <asp:Label runat="server" ID="Headline"></asp:Label> <br/>
            <asp:Panel runat="server" ID="ErrorMessagePanel">
                <p class="error-message-container">
                    <asp:Label runat="server" ID="ErrorMessage"></asp:Label>
                </p>
            </asp:Panel>
        </p>
        <p>
            <asp:Panel runat="server" ID="PagesExportResults">
                <asp:Label runat="server" ID="PagesExportResultsHeadline" CssClass="results-header" Text="Export pages results:"></asp:Label> <br/>
                <asp:Label runat="server" ID="PagesExportResultsInfo"></asp:Label>
            </asp:Panel>
        </p>
        <p>
            <asp:Panel runat="server" ID="ValidationExportResults">
                <asp:Label runat="server" ID="ValidationExportResultsHeadline" CssClass="results-header" Text="Pages validation results:"></asp:Label> <br/>
                <asp:Label runat="server" ID="ValidationExportResultsInfo"></asp:Label>
            </asp:Panel>
        </p>
        <p>
            <asp:Panel runat="server" ID="ForumExportResults">
                <asp:Label runat="server" ID="ForumExportResultsHeadline" CssClass="results-header" Text="Export forum results:"></asp:Label> <br/>
                <asp:Label runat="server" ID="ForumExportResultsInfo"></asp:Label>
            </asp:Panel>
        </p>
        <p>
            <asp:Panel runat="server" ID="FilesExportResults">
                <asp:Label runat="server" ID="FilesExportResultsHeadline" CssClass="results-header" Text="Export files results:"></asp:Label> <br/>
                <asp:Label runat="server" ID="FilesExportResultsInfo"></asp:Label>
            </asp:Panel>
        </p>
    </div>
    </form>
</body>
</html>
