﻿using System;
using System.Collections.Generic;
using System.IO;

using Bonnier.Bt.MultiSite.PageTypes;

using EPiServer;
using EPiServer.Core;

using ExportHelper;
using ExportHelper.Utils;

namespace Bonnier.Bt.MultiSite.Web
{
    public class ExportXmlValidator
    {
        private string validationResultMessageFormat = "All pages have been validated successfully. Validated page ids: {0}.";

        public void ValidateArticlesFromFile(ExportResults result)
        {
            var articlePageIds = GetArticleIdsFromFile();
            ValidateArticlePages(articlePageIds);
            UpdateExportResultValidation(result, articlePageIds);
        }

        private void UpdateExportResultValidation(ExportResults result, string[] pageIds)
        {
            var ids = string.Join(", ", pageIds);
            var repliesInfo = string.Format(validationResultMessageFormat, ids);
            result.ValidationExportResults.Add(repliesInfo);
        }

        private string[] GetArticleIdsFromFile()
        {
            return File.ReadAllLines(ExportSettingsHelper.ArticleValidationIdsFile);
        }

        private void ValidateArticlePages(string[] pageIds)
        {
            var articleValidator = new ExportArticleXmlValidator();

            foreach (var pageId in pageIds)
            {
                var articlePageId = AdjustArticlePageId(pageId);
                var pageReference = new PageReference(articlePageId);
                var articlePage = DataFactory.Instance.GetPage(pageReference) as ArticlePageData;
                if (articlePage == null)
                {
                    throw new Exception(string.Format("Page with id {0} is not Article.", articlePageId));
                }
                if (articlePage.StopPublish <= DateTime.Now)
                {
                    continue;
                }

                articleValidator.ValidatePage(articlePage, ExportSettingsHelper.ArticleXmlSearchMask);
            }
        }

        private string AdjustArticlePageId(string pageId)
        {
            return string.Format(ExportSettingsHelper.ArticlePageIdFormat, pageId);
        }
    }
}