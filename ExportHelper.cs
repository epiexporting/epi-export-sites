﻿using System.Text;

using Bonnier.Common.Forum.Extension;

using EPiServer;
using EPiServer.Community.Forum;
using EPiServer.Core;
using ExportHelper.Utils;

namespace Bonnier.Bt.MultiSite.Web
{
    public static class ExportHelper
    {
        public static string GetFriendlyUrlForString(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return url;
            }

            var pageURLBuilder = new UrlBuilder(url);
            if (!string.IsNullOrEmpty(pageURLBuilder.Host))
            {
                return url;
            }
            var pageId = pageURLBuilder.QueryId;
            if (pageId == null)
            {
                return url;
            }
            
            var pageReference = new PageReference(pageId);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(pageURLBuilder, pageReference, UTF8Encoding.UTF8);
            pageURLBuilder.Host = ExportSettingsHelper.UrlHost;
            return pageURLBuilder.ToString();
        }

        public static string GetContentUrl(PageData pageData)
        {
            var pageUrlBuilder = new UrlBuilder(pageData.LinkURL);
            EPiServer.Global.UrlRewriteProvider.ConvertToExternal(pageUrlBuilder, pageData.PageLink, UTF8Encoding.UTF8);
            pageUrlBuilder.Host = ExportSettingsHelper.UrlHost;
            return pageUrlBuilder.ToString();
        }

        public static string MakeAbsoluteUrl(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return url;
            }

            var pageURLBuilder = new UrlBuilder(url);
            if (!string.IsNullOrEmpty(pageURLBuilder.Host))
            {
                return url;
            }

            pageURLBuilder.Host = ExportSettingsHelper.UrlHost;
            return pageURLBuilder.ToString();
        }

        public static string GetEntityUrl(RoomBase room)
        {
            return MakeAbsoluteUrl(room.GetRoomUri().ToString());
        }

        public static string GetEntityUrl(Topic topic)
        {
            return MakeAbsoluteUrl(topic.GetTopicUri().ToString());
        }
    }
}