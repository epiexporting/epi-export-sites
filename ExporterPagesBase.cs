﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using EPiServer;
using EPiServer.Core;

using ExportHelper;
using ExportHelper.Entities;
using ExportHelper.Utils;

namespace Bonnier.Bt.MultiSite.Web
{
    public abstract class ExporterPagesBase<T, T1, T2>
        where T : PostXmlObject, new()
        where T1 : PageData
        where T2 : List<T>, new()
    {
        private string hrefPattern = "href\\s*=\\s*(?:[\"'](?<1>[^\"']*)[\"']|(?<1>\\S+))";
        private string srcPattern = "src\\s*=\\s*(?:[\"'](?<1>[^\"']*)[\"']|(?<1>\\S+))";
        private string errorFormat = "Error has occurred during the export: \"{0}\"";
        private string exportWithErrorExceptionMessageFormat = "Error completed with {0} errors: {1}";

        private string cdataOpening = "<![CDATA[";
        private string cdataClosing = "]]>";

        protected Dictionary<string, string> KnownUrlIssuesDictionary { get; private set; }

        protected abstract string FileNameFormat { get; }

        protected abstract string PostType { get; }

        private List<string> errors;

        public ExportXmlCreatorBase<T> GetExporter()
        {
            return new ExportXmlCreator<T, T1, T2>(DataFactory.Instance.GetChildren, BuildXmlObject, ExportSettingsHelper.TargetDirectory, FileNameFormat);
        }

        public void ExportPages(ExportResults result)
        {
            KnownUrlIssuesDictionary = ExportSettingsHelper.GetDictionaryFromFile(ExportSettingsHelper.KnownUrlIssueFile);
            errors = new List<string>();
            var exportXmlCreator = new ExportXmlCreator<T, T1, T2>(DataFactory.Instance.GetChildren, BuildXmlObject, ExportSettingsHelper.TargetDirectory, FileNameFormat);

            exportXmlCreator.ExportPages(ExportSettingsHelper.RootPageReference, result);

            if (errors.Count != 0)
            {
                var exceptionMessage = string.Format(exportWithErrorExceptionMessageFormat, errors.Count, string.Join(Environment.NewLine, errors.ToArray()));
                throw new Exception(exceptionMessage);
            }
        }

        public void ExportPagesIgnoreErrors(ExportResults result)
        {
            try
            {
                ExportPages(result);
            }
            catch (Exception ex)
            {
                var error = ex.Message;
            }
        }

        protected virtual T BuildXmlObject(T1 pageData)
        {
            var result = new T();
            result.Id = pageData.PageLink.ToString();
            result.Type = PostType;
            result.Title = pageData.PageName;
            result.Url = ExportHelper.GetContentUrl(pageData);
            result.Permalink = pageData.URLSegment;
            result.Date = pageData.Created;
            return result;
        }

        protected virtual string ParseHtmlProperty(string htmlProperty)
        {
            if (string.IsNullOrEmpty(htmlProperty))
            {
                return null;
            }

            var result = ReplaceHrefs(htmlProperty);

            result = ReplaceSrc(result);

            result = ReplaceInvalidXmlSymbol(result);
            result = ReplaceCdata(result);

            return result;
        }

        protected virtual string ReplaceHrefs(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return null;
            }

            var result = Regex.Replace(inputString, hrefPattern, delegate(Match match) { return string.Format("href=\"{0}\"", GetFriendlyUrlForString(match.Groups[1].ToString())); });

            return result;
        }

        protected virtual string ReplaceSrc(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return null;
            }

            var result = Regex.Replace(inputString, srcPattern, delegate(Match match) { return string.Format("src=\"{0}\"", GetEncodedUrl(match.Groups[1].ToString())); });

            return result;
        }

        protected virtual string GetEncodedUrl(string inputString)
        {
            var parts = inputString.Split("/".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length > 0)
            {
                var fileName = parts[parts.Length - 1];
                if (!string.IsNullOrEmpty(fileName))
                {
                    var decoded = System.Web.HttpUtility.UrlDecode(fileName);
                    ExportLogger.LogMessage(string.Format("initial: {0} encoded: {1}", fileName, decoded));
                    var outString = inputString.Replace(fileName, decoded);
                    return outString;
                }
            }
            return inputString;
        }

        protected virtual string ReplaceInvalidXmlSymbol(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return null;
            }

            inputString = inputString.Replace((char)0x07, ' ');
            inputString = inputString.Replace((char)0x08, ' ');
            return inputString;
        }

        protected virtual string ReplaceCdata(string inputString)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                return null;
            }

            var result = inputString.Replace(cdataOpening, string.Empty).Replace(cdataClosing, string.Empty);
            return result;
        }

        protected virtual string GetFriendlyUrlForString(string url)
        {
            try
            {
                url = HandleKnownUrlIssues(url);
                /*if (!string.IsNullOrEmpty(url) && url.Contains(".jpg"))
                {
                    ExportLogger.LogMessage(url);
                }*/
                return ExportHelper.GetFriendlyUrlForString(url);
            }
            catch (Exception ex)
            {
                errors.Add(string.Format(errorFormat, ex.Message));

                ExportLogger.LogError(string.Format(errorFormat, ex.Message));
                ExportLogger.LogError(string.Format("Failed url: {0}", url));

                return url;
            }
        }

        protected virtual string HandleKnownUrlIssues(string url)
        {
            if (url == "http:///www.thailandliving.se")
            {
                url = "http://www.thailandliving.se";
            }
            if (url == "http://nanny. nu")
            {
                url = "http://nanny.nu";
            }
            if (url == "http://www.charliesbasta- anglar.se")
            {
                url = "http://www.charliesbasta-anglar.se";
            }
            if (url == "http://forsakringskas­san.se")
            {
                url = "http://www.forsakringskassan.se/";
            }

            string urlReplace;
            if (KnownUrlIssuesDictionary.TryGetValue(url, out urlReplace))
            {
                return urlReplace;
            }

            return url;
        }
    }
}