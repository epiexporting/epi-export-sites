﻿using System;
using System.Collections.Generic;

using ExportHelper;
using ExportHelper.Utils;

namespace Bonnier.Bt.MultiSite.Web
{
    public class ExportGlobalSingleExporter
    {
        public static string ExportSettingsFilePath
        {
            get { return @"e:\Export\SkonaHem\SkonaHem Export configs\ExportSettings.txt"; }
        }

        public static ExportResults ExportAll(bool exportArticles, bool exportForum, bool exportFiles, bool validatePages, DateTime? beginDate = null)
        {
            var result = new ExportResults();
            try
            {
                ExportSettingsHelper.InitExportSettingsHelper(ExportSettingsFilePath);
                if (exportArticles)
                {
                    var exporterArticlePages = new ExporterArticlesPages { CreateDateBeginFilter = beginDate };
                    exporterArticlePages.ExportPages(result);
                }

                if (exportForum)
                {
                    /*var exporterForum = new ExportXmlForum(ExportSettings.ForumName, ExportSettings.TargetDirectory, ExportSettings.ForumTargetFileFormat);*/
                    var exporterForum = new ExportXmlForum();
                    exporterForum.ExportForum(ExportHelper.GetEntityUrl, ExportHelper.GetEntityUrl, result);
                }

                if (exportFiles)
                {
                    /*var metadataExporter = new ExportXmlMetadataForVersioningPathProvider(ExportSettings.TargetDirectory, ExportSettings.TargetCopyFilesDirectory, ExportSettings.KnownFileIssueFile);*/
                    var metadataExporter = new ExportXmlMetadataForVersioningPathProvider();
                    /*metadataExporter.CreateXmlWithMetadataInfo(ExportSettings.MetadataRootPathes, ExportSettings.MetadataTargetFile, result);*/
                    metadataExporter.CreateXmlWithMetadataInfo(result, beginDate);
                }

                if (validatePages)
                {
                    var validator = new ExportXmlValidator();
                    validator.ValidateArticlesFromFile(result);
                }
            }
            catch (Exception ex)
            {
                result.SetExceptionMessage(ex);
            }

            return result;
        }
    }
}