﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ExportHelper;

namespace Bonnier.Bt.MultiSite.Web
{
    public partial class ExportPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var result = ExportAll();

            InitHeadline(result);
            InitPagesExportResults(result);
            InitValidationExportResults(result);
            InitForumExportResults(result);
            InitFilesExportResults(result);
        }

        private ExportResults ExportAll()
        {
            bool exportArticles = true;
            bool exportForum = true;
            bool exportFiles = true;
            bool validatePages = true;

            DateTime? beginDate = null;

            var exportQuery = Request.QueryString["export"];
            if (!string.IsNullOrEmpty(exportQuery))
            {
                var exportAreas = exportQuery.Split(',');
                exportArticles = exportAreas.Contains("articles");
                exportForum = exportAreas.Contains("forum");
                exportFiles = exportAreas.Contains("files");
                validatePages = exportAreas.Contains("validate");

                var beginDateParam = Request.QueryString["startDate"];
                if (!string.IsNullOrEmpty(beginDateParam) && (exportArticles || exportFiles))
                {
                    beginDate = Convert.ToDateTime(beginDateParam);
                }
            }

            return ExportGlobalSingleExporter.ExportAll(exportArticles, exportForum, exportFiles, validatePages, beginDate);
        }

        private void InitHeadline(ExportResults result)
        {
            if (result.IsSuccessful)
            {
                Headline.CssClass = "headline success";
                Headline.Text = "Successful export.";
                ErrorMessagePanel.Visible = false;
            }
            else
            {
                Headline.CssClass = "headline failed";
                Headline.Text = "Export failed.";
                ErrorMessagePanel.Visible = true;
                ErrorMessage.Text = HttpUtility.HtmlEncode(result.ExceptionMessage);
            }
        }

        private void InitPagesExportResults(ExportResults result)
        {
            if (!result.PagesExportResults.Any())
            {
                PagesExportResults.Visible = false;
            }
            else
            {
                PagesExportResultsInfo.Text = BuildResultText(result.PagesExportResults);
                PagesExportResults.Visible = true;
            }
        }

        private void InitValidationExportResults(ExportResults result)
        {
            if (!result.ValidationExportResults.Any())
            {
                ValidationExportResults.Visible = false;
            }
            else
            {
                ValidationExportResultsInfo.Text = BuildResultText(result.ValidationExportResults);
                ValidationExportResults.Visible = true;
            }
        }

        private void InitForumExportResults(ExportResults result)
        {
            if (!result.ForumExportResults.Any())
            {
                ForumExportResults.Visible = false;
            }
            else
            {
                ForumExportResultsInfo.Text = BuildResultText(result.ForumExportResults);
                ForumExportResults.Visible = true;
            }
        }

        private void InitFilesExportResults(ExportResults result)
        {
            if (!result.FilesExportResults.Any())
            {
                FilesExportResults.Visible = false;
            }
            else
            {
                FilesExportResultsInfo.Text = BuildResultText(result.FilesExportResults);
                FilesExportResults.Visible = true;
            }
        }

        private string BuildResultText(List<string> resultInfo)
        {
            return string.Join("<br />", resultInfo.ToArray());
        }
    }
}