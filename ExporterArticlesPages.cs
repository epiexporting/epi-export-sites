﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bonnier.Bt.MultiSite.PageTypes;
using Bonnier.Core.Common;
using Bonnier.Widgets.Common.Factbox;
using EPiServer.Core;
using EPiServer.XForms;
using ExportHelper.Entities;
using ExportHelper.Utils;
using PageTypeBuilder;

namespace Bonnier.Bt.MultiSite.Web
{
    public class ExporterArticlesPages : ExporterPagesBase<PostXmlObject, ArticlePageData, PostsListXmlObject>
    {
        protected override string FileNameFormat
        {
            get { return ExportSettingsHelper.ArticleTargetFileFormat; }
        }

        protected override string PostType
        {
            get { return ExportSettingsHelper.ArticlesType; }
        }

        public DateTime? CreateDateBeginFilter { get; set; }
        
        protected override PostXmlObject BuildXmlObject(ArticlePageData pageData)
        {
            if (CreateDateBeginFilter.HasValue && pageData.Created < CreateDateBeginFilter.Value)
            {
                return null;
            }

            var result = base.BuildXmlObject(pageData);
            result.Content = ParseHtmlProperty(pageData.MainBodyCore);
            result.Excerpt = ParseHtmlProperty(pageData.PreambleCore);
            result.Categories = GetCategoriesXmlObject(pageData);
            result.Users = GetUsersXmlObject(pageData);
            result.RichContent = GetRichContentXmlObject(pageData);
            result.FactBoxes = GetFactBoxesXmlObject(pageData);
            result.Meta = GetMetaXmlObject(pageData);
            result.XForm = GetMetaXformObject(pageData);

            return result;
        }

        private PostRichContentXmlObject GetRichContentXmlObject(ArticlePageData pageData)
        {
            if (!HasAnyRichContent(pageData))
            {
                return null;
            }
            
            var result = new PostRichContentXmlObject();
            SetRichContent(result, pageData);

            return result;
        }

        private bool HasAnyRichContent(ArticlePageData pageData)
        {
            return HasImageRichContent(pageData);
        }

        private bool HasImageRichContent(ArticlePageData pageData)
        {
            return !string.IsNullOrEmpty(pageData.ImageCore);
        }
        
        private void SetRichContent(PostRichContentXmlObject richContent, ArticlePageData pageData)
        {
            SetRichContentImage(richContent, pageData);
        }

        private void SetRichContentImage(PostRichContentXmlObject richContent, ArticlePageData pageData)
        {
            if (!HasImageRichContent(pageData))
            {
                return;
            }

            var imageUrl = pageData.ImageCore;
            var photographers = pageData.ImagePhotographerCore == null ? null : new List<string> { pageData.ImagePhotographerCore };
            var title = string.IsNullOrEmpty(pageData.ImageTextCore) ? null : pageData.ImageTextCore;
            richContent.Image = new PostImageXmlObject
            {
                Url = imageUrl,
                Photographers = photographers,
                Title = title
            };
        }

        private List<PostFactBoxXmlObject> GetFactBoxesXmlObject(ArticlePageData articlePageData)
        {
            var factboxBlockDatas = GetFactboxes(articlePageData);

            if (factboxBlockDatas == null || factboxBlockDatas.Count == 0)
            {
                return null;
            }

            var result = factboxBlockDatas.Select(ConvertFactboxBlockDatasToPostFactBoxXmlObject).ToList();
            return result;
        }

        private List<FactboxWidget> GetFactboxes(ArticlePageData articlePageData)
        {
            var factboxesFromArticleTopWidgetZone = GetFactboxesFromWidgetZone(articlePageData.ArticleTopWidgets);
            var factboxesFromBottomWidgetZone = GetFactboxesFromWidgetZone(articlePageData.ArticleBottomWidgets);
            var factboxesFromInRightWidgetZone = GetFactboxesFromWidgetZone(articlePageData.ArticleRightWidgets);
            var factboxesFromTopWidgetZone = GetFactboxesFromWidgetZone(articlePageData.ArticleTopWidgets);

            return CombineFactboxes(factboxesFromArticleTopWidgetZone, factboxesFromBottomWidgetZone, factboxesFromInRightWidgetZone, factboxesFromTopWidgetZone);
        }

        private List<FactboxWidget> CombineFactboxes(params List<FactboxWidget>[] factboxes)
        {
            var result = new List<FactboxWidget>();

            foreach (var factboxesList in factboxes)
            {
                if (factboxesList != null && factboxesList.Count != 0)
                {
                    result.AddRange(factboxesList);
                }
            }

            if (result.Count != 0)
            {
                return result;
            }

            return null;
        }

        private List<FactboxWidget> GetFactboxesFromWidgetZone(PageReferenceCollection widgets)
        {
            var result = new List<FactboxWidget>();
            if (widgets == null)
            {
                return result;
            }

            // TODO: check if it works at all
            var factboxesFromWidgetZone = widgets.Select(x => x.LoadPage<PageData>()).OfType<FactboxWidget>();
            result.AddRange(factboxesFromWidgetZone);
            return result;
        }

        private int GetFactBoxBlockDataContentTypeId()
        {
            var factboxPageTypeId = PageTypeResolver.Instance.GetPageTypeID(typeof(FactboxWidget));
            if (!factboxPageTypeId.HasValue)
            {
                throw new Exception("Cannot get PageTypeId for FactboxWidget.");
            }

            return factboxPageTypeId.Value;
        }

        private PostFactBoxXmlObject ConvertFactboxBlockDatasToPostFactBoxXmlObject(FactboxWidget factboxWidgetPageData)
        {
            var result = new PostFactBoxXmlObject
            {
                Title = factboxWidgetPageData.PageName,
                Content = ParseHtmlProperty(factboxWidgetPageData.Content)
            };

            return result;
        }

        private List<PostCategoryXmlObject> GetCategoriesXmlObject(ArticlePageData articlePageData)
        {
            if (articlePageData != null && articlePageData.SectionCore != null)
            {
                var sectionPageData = articlePageData.SectionCore.LoadPage<PageData>();

                var result = new List<PostCategoryXmlObject>();
                var category = new PostCategoryXmlObject
                {
                    Name = sectionPageData.PageName
                };
                result.Add(category);

                return result;
            }
            else
            {
                return new List<PostCategoryXmlObject>();
            }
        }
        
        private List<PostUserXmlObject> GetUsersXmlObject(ArticlePageData articlePageData)
        {
            var result = new List<PostUserXmlObject>();
            foreach (var author in articlePageData.AuthorsCore)
            {
                var authorPage = author.LoadPage<AuthorPageData>();
                var userXmlObject = new PostUserXmlObject
                {
                    Name = authorPage.PageName,
                    Email = string.IsNullOrEmpty(authorPage.EmailCore) ? null : authorPage.EmailCore
                };
                result.Add(userXmlObject);
            }
            if (!string.IsNullOrEmpty(articlePageData.AltAuthorCore))
            {
                var userXmlObject = new PostUserXmlObject
                {
                    Name = articlePageData.AltAuthorCore
                };
                result.Add(userXmlObject);
            }

            if (result.Count == 0)
            {
                return null;
            }

            return result;
        }

        private PostMetaXmlObject GetMetaXmlObject(ArticlePageData articlePageData)
        {
            if (!HasAnyMetaInfo(articlePageData))
            {
                return null;
            }

            var result = GetPostMetaXmlObject(articlePageData);
            return result;
        }

        private bool HasAnyMetaInfo(ArticlePageData articlePageData)
        {
            return !string.IsNullOrEmpty(articlePageData.SeoDescription)
                || !string.IsNullOrEmpty(articlePageData.SeoKeywords);
        }

        private PostMetaXmlObject GetPostMetaXmlObject(ArticlePageData articlePageData)
        {
            var result = new PostMetaXmlObject();
            FillPostMetaSeoXmlObject(result, articlePageData);

            return result;
        }

        private void FillPostMetaSeoXmlObject(PostMetaXmlObject postMetaXmlObject, ArticlePageData articlePageData)
        {
            var seoInfo = new PostMetaSeoXmlObject();
            if (!string.IsNullOrEmpty(articlePageData.SeoDescription))
            {
                seoInfo.Description = articlePageData.SeoDescription;
            }
            if (!string.IsNullOrEmpty(articlePageData.SeoKeywords))
            {
                seoInfo.Keywords = articlePageData.SeoKeywords;
            }

            postMetaXmlObject.SEO = seoInfo;
        }
        
        private PostXFormXmlObject GetMetaXformObject(ArticlePageData articlePageData)
        {
            if (!HasXform(articlePageData))
            {
                return null;
            }

            var result = GetPostXformXmlObject(articlePageData);
            return result;
        }

        private bool HasXform(ArticlePageData articlePageData)
        {
            var xformGuidString = GetArticleXformString(articlePageData);
            return !string.IsNullOrEmpty(xformGuidString);
        }

        private PostXFormXmlObject GetPostXformXmlObject(ArticlePageData articlePageData)
        {
            var xformGuidString = GetArticleXformString(articlePageData);
            if (string.IsNullOrEmpty(xformGuidString))
            {
                return null;
            }

            var xform = XForm.CreateInstance(new Guid(xformGuidString));
            xform.PageGuid = articlePageData.PageGuid;
            var result = new PostXFormXmlObject
            {
                Id = xform.Id.ToString(),
                Name = xform.FormName,
                Content = xform.DocumentForTransfer
            };
            return result;
        }
        
        private string GetArticleXformString(ArticlePageData articlePageData)
        {
            return articlePageData["XForm"] as string;
        }
    }
}