﻿using EPiServer.Core;

namespace Bonnier.Bt.MultiSite.Web
{
    /*public static class ExportSettings
    {
        public static string TargetDirectory
        {
            get { return @"D:\temp\BT\Export\Vi Foraldrar\Export Vi Foraldrar(draft-1)"; }
        }

        public static string TargetCopyFilesDirectory
        {
            get { return @"D:\temp\BT\Export\Vi Foraldrar\Export Files"; }
        }

        public static string KnownFileIssueFile
        {
            get { return @"D:\temp\BT\Export\Vi Foraldrar\known-file-issues-pathes.txt"; }
        }

        public static string ImagesForceAuthor
        {
            get { return "epiadmin"; }
        }

        public static string MetadataTargetFile
        {
            get { return @"metadata_ViForaldrar(draft-1).xml"; }
        }

        private static string[] metadataRootPathes = new[]
        {
            @"~/Global/Vi_foraldrar"
        };

        public static string[] MetadataRootPathes
        {
            get { return metadataRootPathes; }
        }

        public static string UrlHost
        {
            get { return "www.viforaldrar.se"; }
        }

        private static PageReference rootPageReference = new PageReference(12271);

        public static PageReference RootPageReference
        {
            get { return rootPageReference; }
        }

        public static string ArticleTargetFileFormat
        {
            get { return "ViForaldrar Articles(draft 1)-{0}.xml"; }
        }

        public static string PagesTargetFileFormat
        {
            get { return "ViForaldrar Pages(draft 1)-{0}.xml"; }
        }

        public static string ArticlesType
        {
            get { return "Article"; }
        }

        public static string PagesType
        {
            get { return "FreeHtmlPage"; }
        }

        public static string ArticleValidationIdsFile
        {
            get { return @"D:\temp\BT\Export\Vi Foraldrar\article-validate-ids.txt"; }
        }

        public static string ForumName
        {
            get { return @"viforaldrar"; }
        }

        public static string ForumTargetFileFormat
        {
            get { return "ViForaldrar Forum(draft 1)-{0}.xml"; }
        }

        public static string ArticleXmlSearchMask
        {
            get { return "*Articles*.xml"; }
        }

        public static string ArticlePageIdFormat
        {
            get { return "{0}__ViforaldrarCore"; }
        }
    }*/
}